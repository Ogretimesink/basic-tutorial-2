/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
void TutorialApplication::createCamera(void)
{
    // Create the camera
    mCamera = mSceneMgr->createCamera("PlayerCam");

    // Position the camera
    mCamera->setPosition(Ogre::Vector3(0, 300, 500));

    // Look at a position
    mCamera->lookAt(Ogre::Vector3(0, 0, 0));

	// Distance the camera won't render a mesh
    mCamera->setNearClipDistance(5);

	// Create a camera controller
    mCameraMan = new OgreBites::SdkCameraMan(mCamera);
}
//---------------------------------------------------------------------------
void TutorialApplication::createViewports(void)
{
    // Create a viewport
    Ogre::Viewport* vp = mWindow->addViewport(mCamera);

	// Set the viewport background color
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    // Set the camera aspect ratio to match the viewport width and height
    mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create an instance of a computer graphic model
	Ogre::Entity* ninjaEntity = mSceneMgr->createEntity("ninja.mesh");

	// Allow the entity to cast shadows on its surrounding environment
	ninjaEntity->setCastShadows(true);
 
	// Create a node and add the entity directly to it 
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ninjaEntity);

	// Create a plane object
	Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);

	// Create a mesh using the plane
	Ogre::MeshManager::getSingleton().createPlane("ground", 
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		plane, 
		1500, 1500, 20, 20, 
		true, 
		1, 5, 5, 
		Ogre::Vector3::UNIT_Z);

	// Create an instance of a computer graphic model using the mesh
	Ogre::Entity* groundEntity = mSceneMgr->createEntity("ground");

	// Create a node and add the entity directly to it 
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(groundEntity);

	// Don't allow the entity to cast shadows on its surrounding environment
	groundEntity->setCastShadows(false);

	// Set the material for the model
	groundEntity->setMaterialName("Examples/Rockwall");

	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0, 0, 0));

	// Set the method to generate shadows in the environment 
	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	// Create a directional light that gives shadows
	Ogre::Light* spotLight = mSceneMgr->createLight("SpotLight");

	// Color of light that reflects off scene models
	spotLight->setDiffuseColour(0, 0, 1.0);

	// Shininess of light that reflects off scene models
	spotLight->setSpecularColour(0, 0, 1.0);

	// Set the type of light
	spotLight->setType(Ogre::Light::LT_SPOTLIGHT);

	// Direction to which the light casts
	spotLight->setDirection(-1, -1, 0);

	// Set the position of the light
	spotLight->setPosition(Ogre::Vector3(200, 200, 0));

	// Set angles that determine where the spot light fades to the edge
	spotLight->setSpotlightRange(Ogre::Degree(35), Ogre::Degree(50));

	// Create another directional light that gives shadows
	Ogre::Light* directionalLight = mSceneMgr->createLight("DirectionalLight");

	// Set the type of light
	directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);

	// Color of light that reflects off scene models
	directionalLight->setDiffuseColour(Ogre::ColourValue(0.4f, 0.0f, 0.0f));

	// Shininess of light that reflects off scene models
	directionalLight->setSpecularColour(Ogre::ColourValue(0.4f, 0.0f, 0.0f));

	// Direction to which the light casts
	directionalLight->setDirection(Ogre::Vector3(0, -1, 1));

	// Create another directional light that gives shadows
	Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");

	// Set the type of light
	pointLight->setType(Ogre::Light::LT_POINT);

	// Color of light that reflects off scene models
	pointLight->setDiffuseColour(0.3f, 0.3f, 0.3f);

	// Shininess of light that reflects off scene models
	pointLight->setSpecularColour(0.3f, 0.3f, 0.3f);

	// Set the position of the light
	pointLight->setPosition(Ogre::Vector3(0, 150, 250));
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
